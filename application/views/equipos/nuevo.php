<div class="container">
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
     new equipment
  </b>
</h1>
<br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_equipo" action="<?php echo site_url('equipos/guardarEquipo'); ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-4">
        <label for="nombre_equi" class="form-label white-text"> <b>NOMBRE EQUIPO:</b> </label>
        <input type="text" name="nombre_equi" id="nombre_equi" value="" class="form-control" placeholder="Ingrese el nombre" required>
    </div>
    <div class="col-md-4">
        <label for="siglas_equi" class="form-label white-text"> <b>SIGLAS:</b> </label>
        <input type="text" name="siglas_equi" id="siglas_equi" value="" class="form-control" placeholder="Ingrese las siglas" required>
    </div>
    <div class="col-md-4">
        <label for="fundacion_equi" class="form-label white-text"> <b>FUNDACION:</b> </label>
        <input type="text" name="fundacion_equi" id="fundacion_equi" value="" class="form-control" placeholder="Ingrese el año de fundacion" required>
    </div>
    <div class="col-md-4">
        <label for="region_equi" class="form-label white-text"> <b>REGION:</b> </label>
        <input type="text" name="region_equi" id="region_equi" value="" class="form-control" placeholder="Ingrese la region" required>
    </div>
    
    <div class="col-md-4">
        <label for="numero_titulos_equi" class="form-label white-text"> <b> NÚMERO DE TITULOS:</b> </label>
        <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="" class="form-control" placeholder="Ingrese el número de titulos" required>
    </div>
    <br><br><br>
    <br>
    
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check-circle fa-bounce"></i> Gurdar</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
      </div>

    </div>

</form>

