<br>
<div class="text-center">
    <h1>
      <i class="fas fa-user"></i>
      <b>Position</b>
    </h1>
</div>
<div class="row">
  <div class="col-md-12 text-end">

    <a href="<?php  echo site_url('posiciones/nuevo'); ?>" class="btn btn-outline-success"><i class="fa fa-plus-circle"></i> Agregar Position</a>
    <br> <br>
  </div>

</div>
<?php if ($listadoPosicion): ?>
    <table class="table table-bordered">
        <thead>
              <tr class="text-center">
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DESCRIPCION</th>
                
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoPosicion as $posicion): ?>
                <tr class="text-center">
                  <td><?php echo $posicion->id_pos; ?></td>
                  <td><?php echo $posicion->nombre_pos; ?></td>
                  <td><?php echo $posicion->descripcion_pos; ?></td>
                  
                  <td>
                    <a href="<?php echo site_url('posiciones/editar/').$posicion->id_pos; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                    &nbsp&nbsp
                    <a href="<?php echo site_url('posiciones/borrar/') . $posicion->id_pos; ?>" class="btn btn-danger delete-btn" title="Borrar">
                        <i class="fa-solid fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
    // Script para mostrar el cuadro de diálogo de confirmación antes de eliminar un hospital
    $('.delete-btn').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        Swal.fire({
            title: "CONFIRMACIÓN",
            text: "¿Estás seguro de que deseas eliminar esta Posicion?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Sí",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url; // Redirige al URL de eliminación si se confirma
            }
        });
    });
</script>

    

<?php else: ?>
  <div class="alert alert-danger">
      No se encontro hospitales registrados
  </div>
<?php endif; ?>
