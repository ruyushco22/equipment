
<div class="container">
<h1><i class="fa-solid fa-pen-to-square"></i> <b>Edit equipment</b></h1> <br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_equipo" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id_equi" id="id_equi" value="<?php echo $equipoEditar->id_equi; ?>">
    
    <div class="col-md-4">
        <label for="nombre_equi" class="form-label white-text"> <b>NOMBRE EQUIPO:</b> </label>
        <input type="text" name="nombre_equi" id="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>" class="form-control" placeholder="Ingrese el nombre" required>
    </div>
    <div class="col-md-4">
        <label for="siglas_equi" class="form-label white-text"> <b>SIGLAS:</b> </label>
        <input type="text" name="siglas_equi" id="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>" class="form-control" placeholder="Ingrese las siglas" required>
    </div>
    <div class="col-md-4">
        <label for="fundacion_equi" class="form-label white-text"> <b>FUNDACION:</b> </label>
        <input type="text" name="fundacion_equi" id="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>" class="form-control" placeholder="Ingrese el año de fundacion" required>
    </div>
    <div class="col-md-4">
        <label for="region_equi" class="form-label white-text"> <b>REGION:</b> </label>
        <input type="text" name="region_equi" id="region_equi" value="<?php echo $equipoEditar->region_equi; ?>" class="form-control" placeholder="Ingrese la region" required>
    </div>
    
    <div class="col-md-4">
        <label for="numero_titulos_equi" class="form-label white-text"> <b> NÚMERO DE TITULOS:</b> </label>
        <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>" class="form-control" placeholder="Ingrese el número de titulos" required>
    </div><br><br><br><br>
    
    
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen fa-bounce"></i> &nbsp Editar</button> &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
        </div>
    </div>
</form>


<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $agenciaEditar->latitud; ?>, <?php echo $agenciaEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>
<script>
	$(document).ready(function() {
			// Inicialización del plugin Bootstrap Fileinput
			$("#carnet").fileinput({
					language: 'es',
					maxFileSize: 0
			});
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#formulario_agencia').validate({
        rules: {
            nombre: {
                required: true,
                letras: true,
                minlength: 3,
                primeraLetraMayuscula: true,
            },
            direccion: {
                required: true
            },
            ciudad: {
                required: true,
                letras:true,
                minlength: 3,
                primeraLetraMayuscula: true
            },
            estado: {
                required: true
            },
            telefono: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            fechaApertura: {
                required: true,
                date: true,
                min: "1960-01-01",
                max: "2024-12-31"
            },
            gerente: {
                required: true,
                letras:true,
                minlength: 3,
                formatoNombre: true,
            },
            latitud: {
                required: true,
                number: true
            },
            longitud: {
                required: true,
                number: true

            }
        },
        messages: {
            nombre: {
                required: "Por favor ingrese el nombre de la agencia",
                primeraLetraMayuscula: "La primera letra del nombre debe ser mayúscula",
                minlength: "El nombre debe tener al menos {0} caracteres" // Mensaje de error personalizado para la longitud mínima
            },
            direccion: {
                required: "Por favor ingrese la dirección"
            },
            ciudad: {
                required: "Por favor ingrese la ciudad",
                minlength: "El nombre debe tener al menos {0} caracteres"
            },
            estado: {
                required: "Por favor seleccione el estado"
            },
            telefono: {
                required: "Por favor ingrese el número de teléfono",
                maxlength: "Por favor ingrese el número de 10 digitos",
                minlength: "Por favor ingrese el número de 10 digitos"
            },
            fechaApertura: {
                required: "Por favor seleccione la fecha de apertura",
                max: "Fecha fuera de rango (1960-2024)",
                min: "Fecha fuera de rango (1960-2024)"
            },
            gerente: {
                required: "Por favor ingrese el nombre del gerente",
                  minlength: "El nombre debe tener al menos {0} caracteres",
                formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
            },
            latitud: {
                required: "Por favor ingrese la latitud",
                number: "Ingrese un número válido"
            },
            longitud: {
                required: "Por favor ingrese la longitud",
                number: "Ingrese un número válido"
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.addClass('is-invalid');
            error.insertAfter(element);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    $.validator.addMethod("letras", function(value, element) {
      return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
    }, "Solo se permiten letras");
    $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
          return this.optional(element) || /^[A-Z].*/.test(value);
    }, "La primera letra del nombre debe ser mayúscula");
    $.validator.addMethod("formatoNombre", function(value, element) {
          return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
      }, "Cada palabra debe comenzar con mayúscula");
});

</script>
