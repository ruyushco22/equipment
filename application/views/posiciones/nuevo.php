<div class="container">
<h1>
  <br>
  <b>
    <i class="fa fa-plus-circle"></i>
    New Position
  </b>
</h1>
<br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_posicion" action="<?php echo site_url('posiciones/guardarPosicion'); ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-4">
        <label for="nombre_pos" class="form-label white-text"> <b>NOMBRE:</b> </label>
        <input type="text" name="nombre_pos" id="nombre_pos" value="" class="form-control" placeholder="Ingrese los nombre de la posicion" required>
    </div>
    <div class="col-md-4">
        <label for="descripcion_pos" class="form-label white-text"> <b>DESCRIPCION:</b> </label>
        <input type="text" name="descripcion_pos" id="descripcion_pos" value="" class="form-control" placeholder="Ingrese la descripcion" required>
    </div><br><br><br>
    
    
    
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check-circle fa-bounce"></i> Gurdar</button> &nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
      </div>

    </div>

</form>
<br>
<script type="text/javascript">
    function initMap() {
      var coordenadaCentral = new google.maps.LatLng(-0.17766723173563437, -78.46488534696985);
      var miMapa = new google.maps.Map(
        document.getElementById('mapa'),
        {
          center: coordenadaCentral,
          zoom:8 ,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      var marcador=new google.maps.Marker({
        position:new google.maps.LatLng(-0.1855646473510723, -78.48377334240824),
        map:miMapa,
        title: 'Selecciona la ubicacion',
        draggable:true

      });
      google.maps.event.addListener(marcador,'dragend',
        function(event){
          var latitud=this.getPosition().lat();
          var longitud=this.getPosition().lng();
          document.getElementById('latitud').value = latitud;
          document.getElementById('longitud').value = longitud;
        }
    );
    }
  </script>
  <script>
    $(document).ready(function() {
        // Inicialización del plugin Bootstrap Fileinput
        $("#carnet").fileinput({
            language: 'es',
            maxFileSize: 0
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formulario_corresponsal').validate({
      rules: {
          nombres: {
              required: true,
              letras: true,
              minlength: 3,
              formatoNombre: true
          },
          apellidos: {
              required: true,
              letras: true,
              minlength: 3,
              formatoNombre: true
          },
          direccion: {
              required: true
          },
          ciudad: {
              required: true,
              letras:true,
              minlength: 3,
              primeraLetraMayuscula: true
          },
          telefono: {
              required: true,
              minlength: 10,
              maxlength: 10,
              digits: true
          },
          correo: {
              required: true,
              email: true
          },
          fechaAcuerdo: {
              required: true,
              date: true,
              min: "1960-01-01",
              max: "2024-12-31"
          },
          latitud: {
              required: true,
              number: true
          },
          longitud: {
              required: true,
              number: true
          }
      },
      messages: {
          nombres: {
              required: "Por favor ingrese los nombres",
              letras: "Por favor ingrese solo letras",
              minlength: "El nombre debe tener al menos {0} caracteres",
              formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
          },
          apellidos: {
              required: "Por favor ingrese los apellidos",
              letras: "Por favor ingrese solo letras",
              minlength: "El nombre debe tener al menos {0} caracteres",
              formatoNombre: "Ingrese un nombre válido (cada palabra debe comenzar con mayúscula)",
          },
          direccion: {
              required: "Por favor ingrese la dirección"
          },
          ciudad: {
              required: "Por favor ingrese la ciudad",
              minlength: "El nombre debe tener al menos {0} caracteres"
          },
          telefono: {
              required: "Por favor ingrese el teléfono",
              maxlength: "Por favor ingrese el número de 10 digitos",
              minlength: "Por favor ingrese el número de 10 digitos"
          },
          correo: {
              required: "Por favor ingrese el correo",
              email: "Ingrese un correo válido"
          },
          fechaAcuerdo: {
              required: "Por favor ingrese la fecha del acuerdo",
              max: "Fecha fuera de rango (1960-2024)",
              min: "Fecha fuera de rango (1960-2024)"
          },
          latitud: {
              required: "Por favor ingrese la latitud",
              number: "Ingrese un número válido"
          },
          longitud: {
              required: "Por favor ingrese la longitud",
              number: "Ingrese un número válido"
          }
      },
      errorElement: 'div',
      errorPlacement: function(error, element) {
          error.addClass('invalid-feedback');
          element.addClass('is-invalid');
          error.insertAfter(element);
      },
      highlight: function(element, errorClass, validClass) {
          $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
      }
  });
  $.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
  }, "Solo se permiten letras");
  $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
        return this.optional(element) || /^[A-Z].*/.test(value);
  }, "La primera letra del nombre debe ser mayúscula");
  $.validator.addMethod("formatoNombre", function(value, element) {
        return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
    }, "Cada palabra debe comenzar con mayúscula");
});

</script>
