<?php

class Posiciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Posicion");  
    }


    public function index()
    {
        $data["listadoPosicion"] = $this->Posicion->consultarTodos();  
        $this->load->view("header");
        $this->load->view("posiciones/index", $data);  
        $this->load->view("footer");
    }
    public function nuevo(){
        $this->load->view("header");
        $this->load->view("posiciones/nuevo");
        $this->load->view("footer");
    }
    public function guardarPosicion(){

        $datosNuevoPosicion = array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Posicion->insertar($datosNuevoPosicion);
        $this->session->set_flashdata("confirmacion","Posicion guardada exitosamente");
        redirect('posiciones/index');
    }
    public function borrar($id_pos){
        $this->Posicion->eliminar($id_pos);
        $this->session->set_flashdata("confirmacion","Posicion elimina exitosamente");
        redirect("posiciones/index");
    }

    
    public function editar($id_pos){
        $data["posicionEditar"]=$this->Posicion->obtenerPorId($id_pos);
        $this->load->view("header");
        $this->load->view("posiciones/editar",$data);
        $this->load->view("footer");
      }
      public function actualizarPosicion(){
        $id_pos=$this->input->post("id_pos");
        $datosPosicion=array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Posicion->actualizar($id_pos,$datosPosicion);
        $this->session->set_flashdata("confirmacion",
        "Posicion actualizado exitosamente");
        redirect('posiciones/index');
      }

  
    
}
?>
