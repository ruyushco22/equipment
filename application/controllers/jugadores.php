<?php

class Jugadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");  
    }


    public function index()
    {
        $data["listadoJugadores"] = $this->Jugador->consultarTodos();  
        $this->load->view("header");
        $this->load->view("jugadores/index", $data);  
        $this->load->view("footer");
    }
    public function nuevo(){
        $this->load->view("header");
        $this->load->view("jugadores/nuevo");
        $this->load->view("footer");
    }
    public function guardarPosicion(){

        $datosNuevoPosicion = array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Jugador->insertar($datosNuevoPosicion);
        $this->session->set_flashdata("confirmacion","Jugador guardada exitosamente");
        redirect('jugadores/index');
    }
    public function borrar($id_pos){
        $this->Jugador->eliminar($id_pos);
        $this->session->set_flashdata("confirmacion","Jugador elimina exitosamente");
        redirect("jugadores/index");
    }

    
    public function editar($id_pos){
        $data["posicionEditar"]=$this->Jugador->obtenerPorId($id_pos);
        $this->load->view("header");
        $this->load->view("jugadores/editar",$data);
        $this->load->view("footer");
      }
      public function actualizarPosicion(){
        $id_pos=$this->input->post("id_pos");
        $datosPosicion=array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Jugador->actualizar($id_pos,$datosPosicion);
        $this->session->set_flashdata("confirmacion",
        "Jugador actualizado exitosamente");
        redirect('jugadores/index');
      }

  
    
}
?>
