<div class="text-center">
    <h1>
      <i class="fas fa-building"></i>
      <b>Equipment</b>
    </h1>
</div>
<div class="row">
  <div class="col-md-12 text-end">
    <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-outline-success"><i class="fa fa-plus-circle"></i> Equipment</a>
    <br><br>
  </div>
</div>

<?php if ($listadoEquipos): ?>
    <table class="table table-bordered">
        <thead>
            <tr class="text-center">
                <th>ID</th>
                <th>NOMBRE EQUIPO</th>
                <th>SIGLAS EQUIPO</th>
                <th>FUNDACION</th>
                <th>REGION</th>
                <th>N.TITULOS</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoEquipos as $equipo): ?>
                <tr class="text-center">
                    <td><?php echo $equipo->id_equi; ?></td>
                    <td><?php echo $equipo->nombre_equi; ?></td>
                    <td><?php echo $equipo->siglas_equi; ?></td>
                    <td><?php echo $equipo->fundacion_equi; ?></td>
                    <td><?php echo $equipo->region_equi; ?></td>
                    <td><?php echo $equipo->numero_titulos_equi; ?></td>
                    <td>
                        <a href="<?php echo site_url('equipos/editar/').$equipo->id_equi; ?>" class="btn btn-warning" title="Editar">
                            <i class="fa fa-pen"></i>
                        </a>
                        &nbsp&nbsp
                        <a href="<?php echo site_url('equipos/borrar/').$equipo->id_equi; ?>" class="btn btn-danger delete-btn" title="Borrar">
                            <i class="fa-solid fa-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script type="text/javascript">
    $('.delete-btn').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        Swal.fire({
            title: "CONFIRMACIÓN",
            text: "¿Estás seguro de que deseas eliminar este equipo?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Sí",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url; // Redirige al URL de eliminación si se confirma
            }
        });
    });
    </script>
<?php else: ?>
    <div class="alert alert-danger">
        No se encontraron equipos registrados
    </div>
<?php endif; ?>
