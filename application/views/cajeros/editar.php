
<div class="container">
<h1><i class="fa-solid fa-pen-to-square"></i> <b>EDITAR CAJERO</b></h1> <br>
<form class="row g-3 needs-validation custom-width-form" id="formulario_cajero" action="<?php echo site_url('cajeros/actualizarCajero'); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="idCajero" id="idCajero" value="<?php echo $cajeroEditar->idCajero; ?>">

    <div class="col-md-4">
        <label for="modelo" class="form-label white-text"><b>MODELO:</b></label>
        <input type="text" name="modelo" id="modelo" value="<?php echo $cajeroEditar->modelo; ?>" class="form-control" placeholder="Ingrese el modelo del cajero" required>
    </div>
    <div class="col-md-4">
        <label for="numeroSerie" class="form-label white-text"><b>NÚMERO DE SERIE:</b></label>
        <input type="text" name="numeroSerie" id="numeroSerie" value="<?php echo $cajeroEditar->numeroSerie; ?>" class="form-control" placeholder="Ingrese el número de serie" required>
    </div>
    <div class="col-md-4">
        <label for="ciudad" class="form-label white-text"><b>CIUDAD:</b></label>
        <input type="text" name="ciudad" id="ciudad" value="<?php echo $cajeroEditar->ciudad; ?>" class="form-control" placeholder="Ingrese la ciudad" required>
    </div>
    <div class="col-md-4">
        <label for="fechaInstalacion" class="form-label white-text"><b>FECHA DE INSTALACIÓN:</b></label>
        <input type="date" name="fechaInstalacion" id="fechaInstalacion" value="<?php echo $cajeroEditar->fechaInstalacion; ?>" class="form-control" placeholder="Seleccione la fecha de instalación" required>
    </div>

    <div class="col-md-4">
        <label for="idAgencia" class="form-label white-text"><b>AGENCIA:</b></label>
        <select name="id" id="id" class="form-control" required>
            <option value="">Selecciona una agencia</option>
            <!-- Opción por defecto -->
            <?php foreach ($agencias as $agencia): ?>
            <option value="<?php echo $agencia->id; ?>" <?php if($agencia->id == $cajeroEditar->id) echo "selected"; ?>><?php echo $agencia->nombre; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="col-md-4">
        <label for="estado" class="form-label white-text"><b>ESTADO:</b></label>
        <select name="estado" id="estado" class="form-control" required>
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
        </select>
    </div>
		<script type="text/javascript">
		    // Asegúrate de que $agenciaEditar->estado esté definido y no esté vacío antes de asignarlo al campo <select>
        var estado = "<?php echo isset($cajeroEditar) && isset($cajeroEditar->estado) ? $cajeroEditar->estado : ''; ?>";

		    // Obtén el elemento select por su ID
		    var estadoSelect = document.getElementById('estado');

		    // Verifica que el elemento select exista
		    if (estadoSelect) {
		        // Establece el valor del elemento select con el valor obtenido de PHP
		        estadoSelect.value = estado;
		    } else {
		        console.error('Elemento select no encontrado');
		    }
		</script>
    <div class="row">
        <div class="col-md-6">
            <label for=""> <b>Latitud:</b> </label>
            <input type="number" step="any" name="latitud" id="latitud" value="<?php echo $cajeroEditar->latitud; ?>" class="form-control" placeholder="Ingrese la latitud" readonly>
        </div>
        <div class="col-md-6">
            <label for=""> <b>Longitud:</b> </label>
            <input type="number" step="any" name="longitud" id="longitud" value="<?php echo $cajeroEditar->longitud; ?>" class="form-control" placeholder="Ingrese el longitud" readonly>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-md-12">
            <div id="mapa" style="height:350px; width:100%; border:1px solid black;">
            </div> <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen fa-bounce"></i> &nbsp Editar</button> &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"><i class="fa fa-xmark-circle fa-spin"></i> Cancelar</a>
        </div>
    </div>
</form>



<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $cajeroEditar->latitud; ?>, <?php echo $cajeroEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>
<script>
	$(document).ready(function() {
			// Inicialización del plugin Bootstrap Fileinput
			$("#carnet").fileinput({
					language: 'es',
					maxFileSize: 0
			});
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formulario_cajero').validate({
        rules: {
            modelo: {
                required: true
            },
            numeroSerie: {
                required: true
            },
            ciudad: {
                required: true,
                letras:true,
                minlength: 3,
                primeraLetraMayuscula: true
            },
            fechaInstalacion: {
                required: true,
                date: true,
                min: "1960-01-01",
                max: "2024-12-31"
            },
            id: {
                required: true
            },
            estado: {
                required: true
            },
            latitud: {
                required: true,
                number: true
            },
            longitud: {
                required: true,
                number: true
            }
        },
        messages: {
            modelo: {
                required: "Por favor ingrese el modelo del cajero"
            },
            numeroSerie: {
                required: "Por favor ingrese el número de serie"
            },
            ciudad: {
                required: "Por favor ingrese la ciudad",
                primeraLetraMayuscula: "La primera letra del nombre debe ser mayúscula",
                minlength: "El nombre debe tener al menos {0} caracteres"
            },
            fechaInstalacion: {
                required: "Por favor seleccione la fecha de instalación",
                date: "Por favor ingrese una fecha válida",
                max: "Fecha fuera de rango (1960-2024)",
                min: "Fecha fuera de rango (1960-2024)"
            },
            id: {
                required: "Por favor seleccione una agencia"
            },
            estado: {
                required: "Por favor seleccione el estado"
            },
            latitud: {
                required: "Por favor ingrese la latitud",
                number: "Ingrese un número válido"
            },
            longitud: {
                required: "Por favor ingrese la longitud",
                number: "Ingrese un número válido"
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.addClass('is-invalid');
            error.insertAfter(element);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $.validator.addMethod("letras", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\sáéíóúÁÉÍÓÚüÜñÑ]+$/.test(value);
    }, "Solo se permiten letras");
    $.validator.addMethod("primeraLetraMayuscula", function(value, element) {
          return this.optional(element) || /^[A-Z].*/.test(value);
    }, "La primera letra del nombre debe ser mayúscula");
    $.validator.addMethod("formatoNombre", function(value, element) {
          return this.optional(element) || /^[A-Z][a-z]+\s[A-Z][a-z]+$/.test(value);
      }, "Cada palabra debe comenzar con mayúscula");
});


</script>
