<?php
class Jugador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    
    function insertar($datos){
        $respuesta=$this->db->insert("jugador",$datos);
        return $respuesta;
      }

    function consultarTodos(){
        $jugadores = $this->db->get("jugador");
        if ($jugadores->num_rows() > 0) {
            return $jugadores->result();
        } else {
            return false;
        }
    }
    function eliminar($id_jug){
        $this->db->where("id_pos",$id_pos);
        return $this->db->delete("jugador");
    }
    function actualizar($id_pos,$datos){
        $this->db->where("id_pos",$id_pos);
        return $this->db
                    ->update("jugador",$datos);
    }
    function obtenerPorId($id_pos){
        $this->db->where("id_pos",$id_pos);
        $jugador=$this->db->get("jugador");
        if ($jugador->num_rows()>0) {
          return $jugador->row();
        } else {
          return false;
        }
      }
}
?>
