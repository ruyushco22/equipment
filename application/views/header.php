
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>equipment list</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <!-- Importando de fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Importando de sweetalert-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css">
    <!-- Importando de sweetalert-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    <!-- Importing API de Google maps -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrs4hj_uKCtWnkOfzNRXF2p4_oWSt0IUg&libraries=places&callback=initMap">
		</script>



</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark sticky-top p-0" style="background-color: #d2006e; height: 100px;"">
    <a href="<?php echo site_url();?>" class="navbar-brand d-flex align-items-center border-end px-4 px-lg-5">
        <img src="" alt="" style="width: 200px;">
    </a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav mr-auto p-4 p-lg-0">
            <a href="<?php echo site_url('equipos/index');?>" class="nav-item nav-link active">Equipment</a>
            <a href="<?php echo site_url('posiciones/index');?>" class="nav-item nav-link">Position</a>
            <a href="<?php echo site_url('corresponsales/index');?>" class="nav-item nav-link"></a>
            <a href="<?php echo site_url('mapas/index');?>" class="nav-item nav-link"> </a>
        </div>
    </div>
</nav>



    <?php if ($this->session->flashdata('confirmacion')): ?>
				<script type="text/javascript">
					Swal.fire({
						title: "CORRECTO!",
						text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
						icon: "success"
					});
				</script>
				<?php $this->session->set_flashdata('confirmacion',''); ?>
			<?php endif; ?>
